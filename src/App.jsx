import React from 'react';
import Routes from './routers/Routes.jsx';

import MainPage from './components/pages/MainPage'

function App() {
  return (
    <div>
    <MainPage/>
    <Routes/>
    </div>
  );
}

export default App;

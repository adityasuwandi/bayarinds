import React from 'react'
import mapboxgl from 'mapbox-gl'

mapboxgl.accessToken = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';
export default class Peta extends React.Component {
    constructor(props) {
        super(props);
        this.setState = this.setState.bind(this);
        this.state = {
            idwifi: 0,
            lng: 106.891368, 
            lat: -6.192992,
            zoom: 9
        };
    }

    setwifiLoc() {
        this.props.callbackFromPeta(this.state.idwifi);
    }

    componentDidMount() {
        const { lng, lat, zoom } = this.state;
        var size = 100;
        var pulsingDot = {
            width: size,
            height: size,
            data: new Uint8Array(size * size * 4),

            onAdd: function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                this.context = canvas.getContext('2d');
            },

            render: function () {
                var duration = 1000;
                var t = (performance.now() % duration) / duration;

                var radius = size / 2 * 0.3;
                var outerRadius = size / 2 * 0.7 * t + radius;
                var context = this.context;

                // draw outer circle
                context.clearRect(0, 0, this.width, this.height);
                context.beginPath();
                context.arc(this.width / 2, this.height / 2, outerRadius, 0, Math.PI * 2);
                context.fillStyle = 'rgba(255, 200, 200,' + (1 - t) + ')';
                context.fill();

                // draw inner circle
                context.beginPath();
                context.arc(this.width / 2, this.height / 2, radius, 0, Math.PI * 2);
                context.fillStyle = 'rgba(255, 100, 100, 1)';
                context.strokeStyle = 'white';
                context.lineWidth = 2 + 4 * (1 - t);
                context.fill();
                context.stroke();

                // update this image's data with data from the canvas
                this.data = context.getImageData(0, 0, this.width, this.height).data;

                // keep the map repainting
                map.triggerRepaint();

                // return `true` to let the map know that the image was updated
                return true;
            }
        };
        const map = new mapboxgl.Map({
            container: this.mapContainer,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [lng, lat],
            zoom
        });
        map.on('load', function () {
            map.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });
            // Add a layer showing the places.
            map.addLayer({
                "id": "places",
                "type": "symbol",
                "source": {
                    "type": "geojson",
                    "data": {
                        "type": "FeatureCollection",
                        "features": [{
                            "type": "Feature",
                            "properties": {
                                "description": "<strong>Warung Babeh</strong><p><a href=\"ss\" target=\"_blank\" title=\"Opens in a new window\">Pilih Lokasi Ini",
                                "idwifi": 1
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [106.8249, -6.1753]
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "description": "<strong>Wico Telkom Rawamangun</strong><p><a href=\"ss\" target=\"_blank\" title=\"Opens in a new window\">Pilih Lokasi Ini",
                                "idwifi": 2
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [106.891368, -6.192992]
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "description": "<strong>Wico Telkom Klender</strong><p><a href=\"ss\" target=\"_blank\" title=\"Opens in a new window\">Pilih Lokasi Ini",
                                "idwifi": 3
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [106.8985383, -6.2192434]
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "description": "<strong>Wico Telkom Pondok Kelapa</strong><p><a href=\"ss\" target=\"_blank\" title=\"Opens in a new window\">Pilih Lokasi Ini",
                                "idwifi": 1
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [106.9338338, -6.2373447]
                            }
                        },
                        {
                            "type": "Feature",
                            "properties": {
                                "description": "<strong>Wico Telkom Jatinegara</strong><p><a href=\"ss\" target=\"_blank\" title=\"Opens in a new window\">Pilih Lokasi Ini",
                                "idwifi": 1
                            },
                            "geometry": {
                                "type": "Point",
                                "coordinates": [106.8736168, -6.2204317]
                            }
                        }]
                    }
                },
                "layout": {
                    "icon-image": "pulsing-dot",
                    "icon-allow-overlap": true
                }
            });

            // When a click event occurs on a feature in the places layer, open a popup at the
            // location of the feature, with description HTML from its properties.


            // Change the cursor to a pointer when the mouse is over the places layer.
            map.on('mouseenter', 'places', function () {
                map.getCanvas().style.cursor = 'pointer';
            });

            // Change it back to a pointer when it leaves.
            map.on('mouseleave', 'places', function () {
                map.getCanvas().style.cursor = '';
            });
        });
        map.addControl(new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true
        }));
        map.on('move', () => {

        });
        map.on('click', 'places', (e) => {
            var coordinates = e.features[0].geometry.coordinates.slice();
            var description = e.features[0].properties.description;
            this.setState({
                idwifi: e.features[0].properties.idwifi
            });
            this.setwifiLoc();

            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            new mapboxgl.Popup()
                .setLngLat(coordinates)
                .setHTML(description)
                .addTo(map);
        });

    }

    render() {
        const { lng, lat, zoom } = this.state;
        const style = {
            top: 0,
            bottom: 0,
            height: '250px'
        };
        return (
            <div>
                <div style={{ color: '#ffff' }}>{`Longitude: ${lng} Latitude: ${lat} Zoom: ${zoom}`}</div>
                <div style={style} ref={el => this.mapContainer = el} />
            </div>
        );
    }
}
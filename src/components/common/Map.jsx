import React from 'react'
import mapboxgl from 'mapbox-gl'

mapboxgl.accessToken = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';

export default class Map extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      lng: 106.8249,
      lat: -6.1753,
      zoom: 15
    };
  }

  componentDidMount() {
    const { lng, lat, zoom } = this.state;
    var marker = new mapboxgl.Marker({
      draggable: true
      })
    .setLngLat([lng, lat]);
    const map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom
    });
    map.on('move', () => {
      const { lng, lat } = map.getCenter();
      this.setState({
        lng: lng.toFixed(4),
        lat: lat.toFixed(4),
        zoom: map.getZoom().toFixed(2)
      });
      marker.setLngLat([lng, lat]);
    });
    map.on('click', () => {
      marker.addTo(map)
    });
    map.addControl(new mapboxgl.GeolocateControl({
      positionOptions: {
          enableHighAccuracy: true
      },
      trackUserLocation: true
  }));
    
  }

  render() {
    const { lng, lat, zoom } = this.state;
    const style = {
      top: 0,
      bottom: 0,
      height: '250px'
    };
    return (
      <div>
        <div style={{color:'#ffff'}}>{`Longitude: ${lng} Latitude: ${lat} Zoom: ${zoom}`}</div>
        <div style={style} ref={el => this.mapContainer = el}/>
      </div>
    );
  }
}
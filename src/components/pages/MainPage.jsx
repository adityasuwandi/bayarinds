import React from 'react';
import './../../App.css';
var Ons = require('react-onsenui');

export default class MainPage extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
      };
      this.openpay =this.openpay.bind(this)
    }

      openpay = () => {
        this.props.history.push('/pay');
      };

      render() {
        return (
          <Ons.Splitter>
            <Ons.SplitterContent>
              <Ons.Page>
                <section>
                <Ons.Card style={{backgroundColor:"#01579b", color:"#ffffff"}}>
                  <div className="content">
                  </div>
                </Ons.Card>
                </section>
                <section style={{height:'40%'}}>
                <table style={{width:'100%', height:'100%', marginTop:'15px'}}>
                <tr>
                  <td align='center'>
                    <Ons.Button modifier="quiet" style={{width: '90%', height:'100%', display:'inherit', textAlign:'center'}}>
                      <img src="http://drive.google.com/uc?export=view&id=1sedlEA8-B71A_ZSDnb-FHbWK1m3if1u9"
                       alt="" style={{height:"15%"}}/><p style={{fontSize:'8pt', color:'#01579b'}}>Buy Voucher</p></Ons.Button>
                  </td>
                  <td align='center'>
                    <Ons.Button modifier="quiet" onClick={this.openpay} style={{width: '90%', height:'100%' , display:'inherit', textAlign:'center'}}>
                    <img src="http://drive.google.com/uc?export=view&id=1yZrWaiBJQTBzuQvkYBbRDJRz1ZCoeiJa" 
                      alt="" style={{height:"15%"}}/><p style={{fontSize:'8pt', color:'#01579b'}}>Trouble</p></Ons.Button>
                  </td>
                </tr>
                </table>
                </section>
                <section>
                </section>
              </Ons.Page>
            </Ons.SplitterContent>
          </Ons.Splitter>
        );
      }
  }
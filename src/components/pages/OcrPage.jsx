import React from 'react';
import './../../App.css';
import Peta from '../common/Peta';
import Camera, { FACING_MODES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import mapboxgl from 'mapbox-gl';
import TesseractWorker from 'tesseract.js';
mapboxgl.accessToken = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';
var Ons = require('react-onsenui');
var ons = require('onsenui');


export default class OcrPage extends React.Component{
  
  constructor(props) {
    super(props);
    this.state = {
      url: "",
    };

  }
  
  onTakePhoto (dataUri) {
    TesseractWorker
      .recognize('https://tesseract.projectnaptha.com/img/eng_bw.png')
      .progress((p) => {
        console.log('progress', p);
      })
      .then(({ text }) => {
        console.log(text);
        TesseractWorker.terminate();
      });
    console.log('takePhoto');
  }

  render() {
    return (
      <Ons.Page>
        <h2>{this.props.title}</h2>
        <Camera
          idealFacingMode = {FACING_MODES.ENVIRONMENT}
          onTakePhoto = { (dataUri) => { this.onTakePhoto(dataUri); } }
        />
        <section style={{textAlign:"center"}}>
          <Ons.Button onClick={this.props.changeButton}>Next</Ons.Button>
        </section>
        
      </Ons.Page>
    );
  }
}

